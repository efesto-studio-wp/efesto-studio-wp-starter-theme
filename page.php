<?php

$context = Timber::context();

/**
 * @see https://github.com/serbanghita/Mobile-Detect/wiki/Code-examples
 */
$context['Detect'] = $Detect;
$context['App'] = $App;
$pageOptions = get_field('page_options');
$context['page_options'] = $pageOptions;

// d($pageOptions['private']);
// d($App->userIsLogged());

if ($pageOptions['private'] && !$App->userIsLogged()) {
  // dd('/login');
  header('Location: /login');
  exit;
}

Timber::render('templates/page.twig', $context, $context['cache']['expires']);
