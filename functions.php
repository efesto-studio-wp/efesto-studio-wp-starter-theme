<?php

$config = [
	'context' => [
		'cache_expires' => 0
	],
	// add to $context['menu'][{arrayKey}] a WP menu with name {arrayKey['value']}
	'menu' => [
		// 'twig_key' => 'wp-menu-name'
		'main' => 'main-menu',
		'footer_1' => 'footer-menu-1',
		'footer_2' => 'footer-menu-2',
		'footer_3' => 'footer-menu-3',
	],
	// map url patterns to custom php files
	'urlFileMap' => [
		//'#^my-custom-url/([0-9]+)#' => 'my-custom-file.php'
	],
	'twig' => [
		'functions' => [
			//'twig_key' => 'php_func_name',
			'dd' => 'dd'
		],
		'filters' => [
			'titleTextCtaOptionsTransformer' => 'titleTextCtaOptionsTransformer',
		],
	],
	'wp' => [
		'actions' => [
			'test' => 'test'
		]
	]
];

Efesto::addConfig($config);
Efesto::addContext(['test' => 'testxxxx']);
Efesto::addMenu([
	'service' => 'service-menu',
	'service2' => 'service-menu2',
]);
Efesto::addUrlFileMap([
	'#^test/([0-9]+)#' => 'test.php'
]);
Efesto::addTwigFunction(['pippo'  => 'pippo']);
// Efesto::addTwigFilter(['translate'  => 'translate']);
// Efesto::addTwigExtension(['xatom'  => 'xatom']);
Efesto::addWPActions(['efesto/init' => 'actionx']);
Efesto::addWPFilters(['efesto/init' => 'filterx']);

Efesto::addWPActions([
	'efesto/init' => function () {
		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}
	}
]);

// dd(Efesto::$config['wordpress']['actions']);



Efesto::init();