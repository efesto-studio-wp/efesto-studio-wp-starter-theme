module.exports = {
  map: process.env.NODE_ENV === 'production' ? false : true,
  plugins: {
    "postcss-import-ext-glob": {},
    "postcss-import": {},
    "tailwindcss/nesting": {},
    "tailwindcss": {},
    "autoprefixer": {},
    "cssnano": process.env.NODE_ENV === 'production' ? { preset: 'default' } : false,
    "postcss-advanced-variables": {}
  }
}