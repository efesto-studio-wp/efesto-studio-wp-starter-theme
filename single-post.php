<?php
// post archive page 

$context = Timber::context();

$post_heading = get_field('post_heading');

$post_heading['eyelet'] = (new \Timber\Post($post))->date;
$post_heading['preset'] = 4;
$post_heading['allowed'] = ['title', 'text'];
$post_heading['options']['is_active'] = true;
$post_heading['options']['modifiers'] = ['padding-top'];

$context['post_heading'] = $post_heading;

Timber::render('templates/single-post.twig', $context, $context['cache']['expires']);
