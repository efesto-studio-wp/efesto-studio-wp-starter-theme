const { series, parallel, src, dest, watch } = require('gulp'),
      postcss = require('gulp-postcss'),
      concat = require('gulp-concat'),
      uglify = require('gulp-uglify');
      
const config = {
  fileName: 'app',
  srcPath: 'src/',
  distPath: 'static/',
  viewsPath: 'views/'
}

function css() {
  return src(config.srcPath + 'scss/app.css',)
    .pipe(
      postcss()
    )
    .pipe(dest(config.distPath + '/css'));
}

// todo sourcemaps
function js() {
  return src([
    // 'node_modules/jquery/dist/jquery.js',
    // 'node_modules/jquery-validation/dist/jquery.validate.js',
    // 'node_modules/jquery-circle-progress/dist/circle-progress.min.js',
    // 'node_modules/js-cookie/src/js.cookie.js',
    // 'node_modules/gsap/dist/gsap.js',
    // 'node_modules/gsap/dist/EasePack.js',
    // 'node_modules/gsap/dist/ScrollTrigger.js',
    'node_modules/swiper/swiper-bundle.js',
    config.srcPath + 'js/vendor/*.js',
    config.srcPath + 'js/**/*.js',
    config.viewsPath + '**/*.js',
  ], { allowEmpty: true })
  .pipe(concat(config.fileName + '.js'))
  .pipe(uglify())
  .pipe(dest(config.distPath + '/js'));
}

exports.default = parallel(css, js);
exports.css = css;
exports.js = js;

exports.watch = series(parallel(css, js), w);

// exports.svgmin = svg;

function w() {
  watch([
    config.viewsPath + '**/*.twig',
    config.viewsPath + '**/*.scss',
    config.srcPath + 'scss/application/*.scss',
    config.viewsPath + 'tailwind.safelist.txt',
    'tailwind.config.js'
  ], css);

  watch([
    config.srcPath + 'js/**/*.js',
    config.viewsPath + '**/*.js',
  ], js);
}

function handleError(err) {
  beeper(2);
  log.error('\n\n' + '  |   Error: ' + err.messageOriginal + ' /// line: ' + err.line + '/' + err.column + '\n' + '  |   In file: ' + err.file + '\n');
  this.emit('end');
}
