$(function () {
	new Swiper(".slider-swiper", {
		loop: true,
		autoplay: {
			delay: 3000,
			pauseOnMouseEnter: true
		},
		pagination: {
			el: ".slider-pagination",
			clickable: true,
			renderBullet: function (index, className) {
				return '<div class="' + className + '"><span></div></div>';
			},
		},
	});
});