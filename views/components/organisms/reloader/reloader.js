let width = $(window).width(); 
$(window).on('resize', function() {
    if ($(window).width() == width) return; 
    width = $(window).width();
    showLoader();
});

function showLoader(){
  const reloader = document.querySelector('.reloader');

  if (reloader) {
    reloader.style.display = 'flex';
  }
}