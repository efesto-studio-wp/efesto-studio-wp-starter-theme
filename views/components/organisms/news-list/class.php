<?php

use Detection\MobileDetect;

require_once(WP_CONTENT_DIR . "/mu-plugins/timber-bridge/classes/Component.php");

// extend Component to inherit init method
class News extends Component
{

  public function getNews($args = [], $logged = false)
  {
    $defaultArgs = [
      'post_type'       => 'post',
      'post_status'     => array('publish'),
      'post_per_page'   => 10,
      'paged'   => true,
      'page'   => 1,
      'meta_query' => array(
        // 'relation' => 'AND',
        // 'date' => [
        //   'key' => 'date',
        //   'compare' => '>=',
        //   'value' => date("Y-m-d"),
        //   'type' => 'DATE'
        // ],
        'private' => [
          'key' => 'private',
          'compare' => 'IN',
          'value' => $logged ? ['0', '1', ''] : ['0', '']
        ]
      ),
      'orderby' => array(
        'date' => 'DESC'
      )
    ];

    $args = array_merge($defaultArgs, $args);

    $news = new Timber\PostQuery($args);

    return $news;
  }

  public function news_transformer($post)
  {
    $news = [
      'image' => $post->thumbnail,
      'date' => $post->date,
      'date_label' => $post->date_label,
      'title' => $post->post_title,
      'subtitle' => $post->subtitle,
      'text' => $post->text,
      'link' => $post->link
    ];

    return $news;
  }
}

$News = new News();

add_filter('timber/context', function ($context) use ($News) {
  $context['News'] = $News;

  return $context;
});
