$(function () {
  $(".accordion").each(function () {
    var $this = $(this);

    $this.find(".accordion__title").on("click", function () {
      $this.toggleClass("active");
      $this.siblings(".accordion.active").removeClass("active");
    });
    
  });
});
