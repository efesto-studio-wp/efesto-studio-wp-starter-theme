$(function () {
  $(".load-more").each(function () {
    var $this = $(this);
    var $cta = $this.find(".cta").first();
    var $loadMoreWrapper = $this.parents(".load-more-wrapper").first();
    var loadValue = $loadMoreWrapper.data("load-more-value");

    const updateElements = function ($elements) {
      $elements.find("[data-src]").each(function () {
        var $element = $(this);
        $element
          .attr({
            src: $element.attr("data-src"),
          })
          .removeAttr("data-src");
      });

      $elements.find("[data-srcset]").each(function () {
        var $element = $(this);
        $element
          .attr({
            srcset: $element.attr("data-srcset"),
          })
          .removeAttr("data-srcset");
      });

      $elements.attr("data-load-more", false);
    };

    const displayHiddenElements = function (value) {
      var $hiddenElements = $loadMoreWrapper.find("[data-load-more='true']");

      if (!value || value == -1) {
        updateElements($hiddenElements);
      } else {
        updateElements($hiddenElements.slice(0, value));
      }

      if ($loadMoreWrapper.find("[data-load-more='true']").length <= 0) {
        $this.addClass("hidden");
      }
    };

    $cta.on("click", function (e) {
      e.preventDefault();
      displayHiddenElements(loadValue);
      setTimeout(() => {
        $(window).trigger("resize");
      }, 200);
    });
  });
});
