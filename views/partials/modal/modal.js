// called by componentes embedding the modal to trigger it
function toggleModal(name) {
  // eslint-disable-line no-unused-vars
  $(".modal-" + name + "-container").toggleClass("modal-open");
  $("body").toggleClass("with-modal-open");

  // Refresh vimeo video into modal-video-full-container modals
  $(".modal-" + name + "-container").each(function () {
    let $this = $(this),
      $iframe = $this.find("iframe");

    if (!$iframe.length) {
      return;
    }

    if ($this.hasClass("modal-open")) {
      if ($iframe.data("src")) {
        return;
      }
      $iframe.attr("src", $iframe.data("src"));
    } else {
      $iframe.attr("src", $iframe.data("src"));
    }
  });
}

function onModalClick(event, name) {// eslint-disable-line no-unused-vars
  if (!event.target.classList.contains("modal-closing-target")) return; // clicked on a children
  toggleModal(name);
}

$(function () {
  $(".open-video-modal").on("click", function (event) {
    event.preventDefault();
    let target = $(this).prop("target");
    toggleModal(target); // eslint-disable-line no-undef
  });
});
