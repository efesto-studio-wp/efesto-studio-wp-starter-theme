$(function () {
  // set navbar animation start considering the presence of a st scene as first component
  setTimeout(() => {
    $("[class*=navbar-header]").each(function () {
      let $navbar = $(this);

      let $component = $("section.component [data-st-duration").first();
      let stDuration = $component.data("st-duration");

      const scrolltriggerStart = () => {
        let start = stDuration
          ? `top+=${stDuration - $navbar.height() / 2}px top`
          : "top top";

        return start;
      };

      let duration = "150px";
      setTimeout(() => {
        let anim = gsap.timeline({
          defaults: {
            ease: "steps(1)",
            duration: 2,
          },
        });
        if ($("header.header").hasClass("menu--light-transparent")) {
          // white on transparent -> white on black
          anim = anim.to(
            // navbar becomes black
            $navbar,
            {
              css: {
                backgroundColor: "rgba(0, 0, 0, 0.9)",
              },
            },
            0
          );
        } else {
          // white on transparent -> black on white
          anim = anim
            .to(
              // navbar becomes white, text becomes black
              $navbar,
              {
                css: {
                  backgroundColor: "#fff",
                  color: "#000",
                },
              },
              0
            )
            .to(
              // eCalssic logo becomes black
              $navbar.find("svg path"),
              {
                fill: "#000",
              },
              0
            )
            .to(
              // the pill border becomes black
              $navbar.find(".menu-item-pill a"),
              {
                css: {
                  borderColor: "#000",
                },
              },
              0
            )
            .to(
              // the hamburger becomes black
              $navbar.find("#menu-toggle span"),
              {
                css: {
                  backgroundColor: "#000",
                },
              },
              0
            );
        }
        ScrollTrigger.create({
          animation: anim,
          invalidateOnRefresh: true,
          start: () => scrolltriggerStart(),
          end: () => duration,
          refreshPriority: $("section.component").last().data("index") * 2 * -1,
          scrub: 0.5,
        });
      }, 500);
    });
  }, 400);
});
