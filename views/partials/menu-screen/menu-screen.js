$(function () {
  $('.menu-screen').removeClass('hidden');

  $('#menu-toggle').on('click', function() {
    $('body').toggleClass('body--menu-screen-opened');
  });
});