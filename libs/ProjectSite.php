<?php
require_once(WPMU_PLUGIN_DIR . '/timber-bridge/classes/EfestoSite.php');
require_once('ProjectFunctions.php');


class ProjectSite extends EfestoSite
{
  use ProjectFunctions;

  private $config = [];

  public function __construct($config = [])
  {
    $this->config = array_merge_recursive($this->config, $config);
    parent::__construct($this->config);
  }

  function userIsLogged()
  {
    return is_user_logged_in();
  }

  function getUserRoles()
  {
    if (!is_user_logged_in()) {
      return false;
    }
    global $current_user;

    return $current_user->roles;
  }

  function hasUserRoles($roles = null)
  {
    $roles = is_string($roles) ? [$roles] : $roles;

    return !empty(array_intersect($roles, $this->getUserRoles()));
  }
}
