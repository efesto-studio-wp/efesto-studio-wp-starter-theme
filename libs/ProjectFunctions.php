<?php

trait ProjectFunctions
{

  public function getFileMTime($file_path = false)
  {
    $file_date = false;
    if (file_exists(get_template_directory() . $file_path)) {
      $file_date = filemtime(get_template_directory() . $file_path);
    }

    return $file_date;
  }

  public function titleTextCtaOptionsTransformer($componentOptions)
  {

    $map = [
      'ff-roboto' => 'font-roboto',
      'fs-60' => 'h1-24 md:h1-60',
      'w-2' => 'col-span-12 md:col-span-2',
      'w-3' => 'col-span-12 md:col-span-3',
      'w-4' => 'col-span-12 md:col-span-4',
      'w-5' => 'col-span-12 md:col-span-5',
      'w-6' => 'col-span-12 md:col-span-6',
      'w-7' => 'col-span-12 md:col-span-7',
      'w-8' => 'col-span-12 md:col-span-8',
      'w-9' => 'col-span-12 md:col-span-9',
      'w-10' => 'col-span-12 md:col-span-10',
      'w-12' => 'col-span-12',
      'o-0' => 'md:col-start-1',
      'o-1' => 'md:col-start-2',
      'o-2' => 'md:col-start-3',
      'o-3' => 'md:col-start-4',
      'o-4' => 'md:col-start-5',
      'o-5' => 'md:col-start-6',
      'o-6' => 'md:col-start-7',
      'o-7' => 'md:col-start-8',
      'o-8' => 'md:col-start-9',
      'o-9' => 'md:col-start-10',
      'o-10' => 'md:col-start-11',
      'm-xs' => 'mb-2',
      'm-sm' => 'mb-4',
      'm-md' => 'mb-6',
      'm-l' => 'mb-8',
      'm-xl' => 'mb-12',
      'm-2xl' => 'mb-16',
      'm-3xl' => 'mb-24',
    ];

    $options = [];
    $options['font'] = $map[$componentOptions['font']] ?? false;
    $options['offset'] = $map[$componentOptions['offset']] ?? false;
    $options['width'] = $map[$componentOptions['width']] ?? false;
    $options['margin'] = $map[$componentOptions['margin']] ?? false;
    $options['size'] = $map[$componentOptions['size']] ?? false;

    // d($options);

    return $options;
  }

  // Override default theme support function
  // public function addThemeSupport()
  // {
  //   add_theme_support('title-tag');
  //   add_theme_support('post-thumbnails');
  //   add_theme_support('html5');
  //   add_theme_support('menus');
  // }
}
