const plugin = require("tailwindcss/plugin");

module.exports = {
  mode: "jit",

  purge: {
    content: [
      "./views/**/*.scss",
      "./views/**/*.twig",
      "./views/tailwind.safelist.txt",
    ],
  },

  theme: {
    /**
     * Set default container behaviour
     */
    container: {
      center: true,
      padding: "1.5rem",
    },

    colors: {
      //Standard main colors naming start
      primary: "#d4481f",
      secondary: "#04554f",
      success: "#28a745",
      danger: "#dc3545",
      warning: "#ffc107",
      black: "#000000",
      white: "#ffffff",
      //Standard main colors naming end
    },

    fontFamily: {
      // Efesto standard font-family naming convention (family-1, family-2 ...)
      "family-1": ["Montserrat", "Arial"],
      "family-2": ["Roboto", "Arial"],
    },

    modifiers: ['dark', 'light'],

    extend: {},
  },

  variants: {},

  plugins: [
    require("@tailwindcss/line-clamp"),
    require('@tailwindcss/aspect-ratio'),
    
    /**
     * Add modifiers using "Class Strategy" based on theme.modifiers attribute (array)
     * ex: modifiers: ["light"] generate ` --light .--light:<foo-class>` selector
     */
     plugin(function ({ addVariant, e, theme }) {
      const modifier = theme("modifiers");

      if (modifier && modifier.constructor == Array) {
        modifier.forEach(function (modifier) {
          let modifierName = `--${e(modifier)}`;

          addVariant(`${modifierName}`, ({ modifySelectors, separator }) => {
            modifySelectors(({ className }) => {
              return `.${modifierName} .${e(
                `${modifierName}${separator}${className}`
              )}`;
            });
          });
        });
      }
    }),

    /**
     * Add center utilities
     */
    plugin(function ({ addUtilities }) {
      const newUtilities = {
        ".center-absolute-y": {
          position: "absolute",
          top: "50%",
          transform: "translateY(-50%)",
        },
        ".center-absolute-x": {
          position: "absolute",
          left: "50%",
          transform: "translateX(-50%)",
        },
        ".center-absolute": {
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%, -50%)",
        },
        ".center-flex-y": {
          display: "flex",
          "align-items": "center",
        },
        ".center-flex-x": {
          display: "flex",
          "justify-content": "center",
        },
        ".center-flex": {
          display: "flex",
          "align-items": "center",
          "justify-content": "center",
        },
      };

      addUtilities(newUtilities, ["responsive"]);
    }),
  ],
};