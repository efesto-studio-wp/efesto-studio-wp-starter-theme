gsap.registerEffect({
  name: "blur",
  effect: (targets, config) => {
    var blurValue = config.blur || '0.25rem';

    return gsap.to(targets, {
      duration: config.duration,
      stagger: config.stagger,
      ease: config.ease,
      css: {
        filter: `blur(${blurValue})`
      }
    });
  },
  defaults: {
    duration: 1,
    stagger: false,
    ease: Linear.easeNone,
  },
  extendTimeline: true,
});
