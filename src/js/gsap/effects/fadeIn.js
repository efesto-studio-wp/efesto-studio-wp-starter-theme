gsap.registerEffect({
  name: "fadeIn",
  effect: (targets, config) => {
    return gsap.to(targets, {
      duration: config.duration,
      stagger: config.stagger,
      ease: "power3.out",
      overwrite: "auto",
      opacity: 1,
      delay: config.delay,
    });
  },
  defaults: {
    delay: 0.3,
    duration: 1,
    stagger: false,
  },
  extendTimeline: true,
});

gsap.registerEffect({
  name: "fadeInMoving",
  effect: (targets, config) => {
    return gsap.to(targets, {
      duration: config.duration,
      ease: config.ease,
      stagger: config.stagger,
      overwrite: "auto",
      opacity: 1,
      yPercent: "0",
      xPercent: "0",
      x: "0",
      y: "0",
      rotate: "0",
      delay: config.delay,
      ...config.properties
    });
  },
  defaults: {
    ease: "power3.out",
    delay: 0.3,
    duration: 1,
    stagger: false,
  },
  extendTimeline: true,
});

gsap.registerEffect({
  name: "borderFadeIn",
  effect: (targets, config) => {

    return gsap.to(targets, {
      duration: config.duration,
      stagger: config.stagger,
      ease: config.ease,
      delay: config.delay,
      css: {
        "--tw-border-opacity": 1,
      }
    });
  },
  defaults: {
    delay: 0.3,
    duration: 0.3,
    stagger: false,
    ease: Linear.easeNone,
  },
  extendTimeline: true,
});

gsap.registerEffect({
  name: "alphaIn",
  effect: (targets, config) => {
    return gsap.to(targets, {
      duration: config.duration,
      stagger: config.stagger,
      reversed: config.reversed,
      ease: "power3.out",
      overwrite: "auto",
      autoAlpha: config.autoAlpha,
      delay: config.delay,
    });
  },
  defaults: {
    delay: 0,
    duration: 0.8,
    stagger: false,
    autoAlpha: 1,
  },
  extendTimeline: true,
});