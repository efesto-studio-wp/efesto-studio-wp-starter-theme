gsap.registerEffect({
  name: "counter",
  extendTimeline: true,
  defaults: {
    from: 0,
    to: 0,
    ease: "power3",
    increment: 1,
  },
  effect: (targets, config) => {
    let tl = gsap.timeline();
    let num = targets[0].innerText.replace(/,/g, "");
    targets[0].innerText = num;

    tl.fromTo(
      targets,
      { innerText: config.from },
      {
        duration: config.duration,
        innerText: config.to,
        //snap:{innerText:config.increment},
        modifiers: {
          innerText: function (innerText) {
            return gsap.utils
              .snap(config.increment, innerText)
              .toString()
              .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          },
        },
        ease: config.ease,
      },
      0
    );

    return tl;
  },
});