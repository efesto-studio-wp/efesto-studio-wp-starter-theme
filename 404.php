<?php

$context = Timber::context();

Timber::render('templates/404.twig', $context, $context['timber_cache_time']);
